package pl.greatidea.treasurehunter.model;

import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "museum")
public class Museum {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "museum_id")
    private long id;

    @NotNull
    private String name;

    @NotNull
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "museum_address_id")
    private MuseumAddress museumAddress;


    @OneToMany(mappedBy = "museum", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<MuseumTransfer> museumTransfers;

    public Museum() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public MuseumAddress getMuseumAddress() {
        return museumAddress;
    }

    public void setMuseumAddress(MuseumAddress museumAddress) {
        this.museumAddress = museumAddress;
    }

    public List<MuseumTransfer> getMuseumTransfers() {
        return museumTransfers;
    }

    public void setMuseumTransfers(List<MuseumTransfer> museumTransfers) {
        this.museumTransfers = museumTransfers;
    }
}
