package pl.greatidea.treasurehunter.model;

import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "tresure")
public class Tresure {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "tresure_id")
    private long id;

    @NotNull
    private String name;

    @NotNull
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "category_id")
    private Category category;

    @NotNull
    private Date createDate;

    @NotNull
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "user_id")
    private User user;

    private String descrition;
    private Integer year;

    private String gpsLocalization;

    @OneToOne(mappedBy = "tresure")
    private MuseumTransfer museumTransfer;

    @OneToMany(mappedBy = "tresure", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Photo> photoList;

    public Tresure() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getDescrition() {
        return descrition;
    }

    public void setDescrition(String descrition) {
        this.descrition = descrition;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public String getGpsLocalization() {
        return gpsLocalization;
    }

    public void setGpsLocalization(String gpsLocalization) {
        this.gpsLocalization = gpsLocalization;
    }

    public MuseumTransfer getMuseumTransfer() {
        return museumTransfer;
    }

    public void setMuseumTransfer(MuseumTransfer museumTransfer) {
        this.museumTransfer = museumTransfer;
    }

    public List<Photo> getPhotoList() {
        return photoList;
    }

    public void setPhotoList(List<Photo> photoList) {
        this.photoList = photoList;
    }
}
