package pl.greatidea.treasurehunter.repository;

import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;

import pl.greatidea.treasurehunter.model.User;

@Transactional
public interface UserRepository extends CrudRepository<User, Long> {

    User findOneByEmail(String email);

    User findOneByEmailAndPassword(String email, String password);

    User findUserById(Long id);
}